/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;



/**
 *
 * @author Miguel
 */
public class Estadisticas {

    private String alumno;
    private double mayor;
    private double promedio;
   
    /*
    Método que calcula la sumatoria de n
     */
    public void Promedio(double cali[], int n) {
        for (int i = 0; i < n; i++) {
            promedio += cali[i] / n;
        }
        System.out.println("El promedio grupal es de: " + promedio);
    }

    /*
    Metodo que calcula el promedio mayor
     */
    public void mayor(double cali[], String Alumnos[]) {
        for (int i = 0; i < cali.length; i++) {
            if (cali[i] > mayor) {
                mayor = cali[i];
                alumno = Alumnos[i];
            }
        }
        System.out.println("Calificación más Alta " + mayor + " Alumno: " + alumno);
    }

    public void Imprimir(double Cali[], String Alumnos[]) {
        System.out.printf("%-30s %-30s%n%n", "Alumnos", "Calificacion");
        for (int i = 0; i < Cali.length; i++) {
            System.out.printf("%-30s %-30f%n", Alumnos[i], Cali[i]);
        }
    }
}
