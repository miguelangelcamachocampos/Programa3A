/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Scanner;

/**
 *
 * @author Miguel
 */
public class TestEstadisticas {

    public static void main(String[] args) {
        Estadisticas e = new Estadisticas();
        Scanner Teclado = new Scanner(System.in);
        System.out.println("¿Grupo?");
        String grupo = Teclado.nextLine();
        System.out.println("¿Alumnos?");
        int n = Teclado.nextInt();
        double Calificaciones[] = new double[n];
        String Alumnos[] = new String[n];
        for (int i = 0; i < Calificaciones.length; i++) {
            System.out.println("¿Calificación?");
            Calificaciones[i] = Teclado.nextDouble();
            System.out.println("Alumno:");
            Alumnos[i] = Teclado.next();
        }

        e.Promedio(Calificaciones, n);
        e.mayor(Calificaciones, Alumnos);
        e.Imprimir(Calificaciones, Alumnos);
        System.out.println("Calificaciones ingresadas:" + n);
    }
}
